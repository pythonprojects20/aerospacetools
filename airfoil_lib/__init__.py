# __init__.py

from .Airfoil import Airfoil
from .NACA1 import NACA1
from .NACA4 import NACA4
from .NACA5 import NACA5
from .NACA6 import NACA6
from .NACA7 import NACA7
from .NACA8 import NACA8
