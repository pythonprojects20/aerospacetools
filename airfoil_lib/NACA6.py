# NACA8.py

from airfoil_lib import Airfoil
import numpy as np


class NACA6(Airfoil):
    """Contains properties and methods for NACA 6 and 6A-Series Airfoils."""

    def __init__(self, number, chord, iterations=1000, a=1):
        super().__init__(number, chord, iterations)
        self.T = float(self.number[-1:-3:-1][::-1]) / 100
        self.cl = float(self.number[-3]) / 10
        self.P = float(self.number[1]) * (self.c / 10)
        if (self.number.find('A') != -1) or (self.number.find('a') != -1):  # Checking for A-Series airfoil
            self.a = 0.8
        else:
            self.a = a
        check = 0
        for x in range(10):  # If there are 6 numbers then there is a Cl range in there
            check += self.number.count('{}'.format(x))
            if check == 6:
                self.cl_range = float(self.number[2]) / 10
            else:
                self.cl_range = 0

    def __repr__(self):
        """ This method recreates the input for this class"""
        return 'airfoil_lib.NACA6({}, {}, {}, {})'.format(self.number, self.c, self.iter, self.a)

    def __str__(self):
        """This method prints the properties of the NACA 6 series airfoil given."""
        if self.a == 1:
            msg = "This airfoil, NACA {}, is a NACA 6-Series airfoil with uniform loading along the chord. The " \
                  "chordwise position of minimum pressure is located at {:.1f} of the chord behind the\nleading " \
                  "edge. The design lift coefficient is Cl = {:.2f}, with a range of +/- {:.1f} where a favorable " \
                  "pressure gradient exists on both surfaces. The thickness is {:.2f}% of the chord." \
                .format(self.number, self.P, self.cl, self.cl_range, self.T)
        elif (self.number.find('A') != -1) or (self.number.find('a') != -1):
            msg = "This airfoil, NACA {}, is an 6A series NACA airfoil with an empirical modification of a = 8 " \
                  "camberline. The chordwise position of minimum pressure is located at {:.1f} of the\nchord behind " \
                  "the leading edge. The design lift coefficient is Cl = {:.2f}, with a range of +/- {:.1f} where a " \
                  "favorable pressure gradient exists on both surfaces. The thickness is\n{:.2f}% of the chord." \
                .format(self.number, self.P, self.cl, self.cl_range, self.T)
        else:
            msg = "This airfoil, NACA {}, is an 6 series NACA airfoil with a mean camberline designation of a = {}. " \
                  "The chordwise position of minimum pressure is located at {:.1f} of the\nchord behind the leading " \
                  "edge. The design lift coefficient is Cl = {:.2f}, with a range of +/- {:.1f} where a favorable " \
                  "pressure gradient exists on both surfaces. The thickness is\n{:.2f}% of the chord." \
                .format(self.number, self.a, self.P, self.cl, self.cl_range, self.T)
        return msg

    def coordinate(self, x):
        """This method calculates the camber and gradients of the airfoil."""
        if self.a == 1:  # If a = 1 then these are the equations used. Otherwise use the equations in else block
            y_c = -(self.cl / (4 * np.pi)) * (
                        (1 - x / self.c) * np.log(1 - x / self.c) + (x / self.c) * np.log(x / self.c))
            dyc_dx = (self.cl / (4 * np.pi)) * (np.log(1 - x / self.c) - np.log(x / self.c))
        else:
            g = (-1 / (1 - self.a)) * (self.a ** 2 * (.5 * np.log(self.a) - .25) + 0.25)
            h = (1 - self.a) * (0.5 * np.log(1 - self.a) - 0.25) + g
            if (self.number.find('A') != -1) or (self.number.find('a') != -1) and (0 <= x / self.c < 0.87437):
                # Checking for A-Series. Use this Cl forward max thickness
                self.cl /= 1.0209
            if (self.number.find('A') != -1) or (self.number.find('a') != -1) and (0.87437 < x / self.c <= 1):
                # Checking for A-Series. Use these equations for aft max thickness of airfoil
                y_c = 0.0302164 * self.cl - 0.245209 * (x / self.c - 0.87437) * self.cl
                dyc_dx = -0.245209 * self.cl
            else:  # These are the general equations for the 6-Series
                const = self.cl / (2 * np.pi * (1 + self.a))
                y_c = const * ((1 / (1 - self.a)) * (
                            .5 * (self.a - x / self.c) ** 2 * np.log(np.absolute(self.a - x / self.c)) -
                            .5 * (1 - x / self.c) ** 2 * np.log(1 - x / self.c) + .25 * (1 - x / self.c) ** 2 - .25 *
                            (self.a - x / self.c) ** 2) - (x / self.c) * np.log(x / self.c) + g - h * (x / self.c))
                dyc_dx = const * (
                            (1 / (1 - self.a)) * ((1 - x / self.c) * np.log(1 - x / self.c) - (self.a - x / self.c) *
                                                  np.log(np.absolute(self.a - x / self.c))) -
                            np.log(x / self.c) - 1 - h)
        return y_c, dyc_dx
