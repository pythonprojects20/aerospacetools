# Aerospace Tools

The purpose of this project is to create a set of tools similar to Xfoil for use in aerodynamics. This will begin with 
airfoil tools and branch off from there. This is my first project on any git system as well as my first python project.

## Getting Started

I think you can just download and install the python files any way you know how right now. When I get more familiar with 
this system I'll add better instructions.

### Prerequisits

The programs need Numpy and Matplotlib to function correctly. Also a graphical package such as tkinter for matplotlib to 
function correctly.

### Installing

This section is under constructon

### Contributing

This section is under constructon

## Versioning

This section is under constructon

## Authors

* **Chris McGlown Jr.** - *Initial Work* - [CMcGlownJr](https://gitlab.com/cmcglownjr)

## License

This project is licensed under the MIT License

## Acknowledgements

All the great engineers who wrote the books that I'm using for this project.
